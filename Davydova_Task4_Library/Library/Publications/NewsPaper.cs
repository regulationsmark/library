﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.Library.Publications
{
    class NewsPaper:Publication
    {
        private int IssueNumber { get; set; } //closed property?
        private DateTime issueDate;
        private string iSSN;

        public NewsPaper(string name, int countOfPages, string note, string placeOfPublication, string publishingHouse, int publishingYear, int issueNumber, DateTime issueDate, string iSSN)
            :base(name, countOfPages, note, placeOfPublication, publishingHouse, publishingYear)
        {
            IssueNumber = issueNumber;
            IssueDate = issueDate;

        }

        public DateTime IssueDate
        {
            get
            {
                return issueDate;
            }
            set
            {
                if (base.PublishingYear != value.Year)
                {
                    throw new ArgumentOutOfRangeException($"{nameof(value)} year must match with publication year of newspaper");
                }
                issueDate = value;

            }
        }

        public string ISSN
        {
            get
            {
                return iSSN;
            }
            set
            {
                //add all chacking
                iSSN = value;
            }
        }


    }
}
