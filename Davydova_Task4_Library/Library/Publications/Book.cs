﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.OtherEntities;

namespace Davydova_Task4_Library.Library.Publications
{
    public class Book : Publication
    {
        public List<Author> Authors { get; set; }
        private string iSBN;

        public Book(string name, int countOfPages, string note, string placeOfPublication, string publishingHouse, int publishingYear, List<Author> authors, string iSBN) 
            : base(name, countOfPages, note, placeOfPublication, publishingHouse, publishingYear)
        {
            Authors = authors;
            ISBN = iSBN;
        }

        public string ISBN
        {
            get
            {
                return iSBN;
            }
            set
            {
                //all checking value
                iSBN = value;
            }
        }
    }
}
