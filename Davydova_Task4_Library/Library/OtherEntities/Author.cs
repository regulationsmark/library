﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.Library.OtherEntities
{
    public class Author
    {
        private string name;

        private string surname;
        public Author(string name, string surname)
        {
            Name = name;
            Surname = surname;
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                //add all checking
                name = value;
            }
        }

        public string Surname
        {
            get
            {
                return surname;
            }
            set
            {
                //add all checking
                surname = value;
            }
        }


    }
}
