﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Davydova_Task4_Library.Library.OtherEntities;

namespace Davydova_Task4_Library.Library.Documents
{
    class Patent:LibraryObject
    {
        private List<Author> Inventors { get; set; }

        private int RegistrationNumber { get; set; }
        private DateTime ApplicationDate { get; set; }

        private DateTime publicationDate;

        public Patent(string name, int countOfPages, string note, string placeOfPublication, List<Author> inventors, int registrationNumber, DateTime applicationDate, DateTime publicationDate) 
            :base(name, countOfPages, note)
        {
            PlaceOfPublication = placeOfPublication;
            Inventors = inventors;
            RegistrationNumber = registrationNumber;
            ApplicationDate = applicationDate;
            PublicationDate = publicationDate;

        }

        public override string PlaceOfPublication
        {
            get
            {
                return PlaceOfPublication;
            }
            set
            {
                PlaceOfPublication = value; //add all checking
            }
        }
        
        public DateTime PublicationDate
        {
            get
            {
                return publicationDate;
            }
            set
            {
                DateTime curDate = DateTime.Now;
                if (value < ApplicationDate || value > curDate)
                {
                    throw new ArgumentOutOfRangeException($"{nameof(value)} cannot be earlier than the filing of the application and more than the current date");
                }
                publicationDate = value;
            }
        }
    }
}
