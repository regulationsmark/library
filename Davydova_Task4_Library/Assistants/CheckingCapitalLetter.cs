﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Davydova_Task4_Library.Assistants
{
    class CheckingCapitalLetter
    {
        private bool CheckingCapitelLetter(string checkingString)
        {
            bool isCorrectName = false;
            foreach (char item in checkingString)
            {
                if (isCorrectName && Char.IsLetter(item) && Char.IsUpper(item))
                {
                    isCorrectName = false;
                }
                else if (Char.IsLetter(item) && Char.IsUpper(item))
                {
                    isCorrectName = true;
                }
            }
            return isCorrectName;
        }
    }
}
